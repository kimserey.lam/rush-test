import { Person, testconst } from 'my-models';
import React from 'react';
import './App.css';

function App() {

  const p = new Person();
  p.firstName = "tom";

  return (
    <div className="App">
      <header className="App-header">
        Hello heheh {p.firstName} {testconst}
      </header>
    </div>
  );
}

export default App;
