import express from "express";
import { Person } from "my-models";
import "reflect-metadata";
import { createConnection, ConnectionOptionsReader } from "typeorm";

async function test() {
  const opts = await new ConnectionOptionsReader({
    root: __dirname + "/..",
  }).get("default");

  new ConnectionOptionsReader({
    root: __dirname + "/..",
  })
    .all()
    .then((xs) => console.log(xs));

  const conn = await createConnection(opts);

  const person = new Person();
  person.firstName = "kim";
  await conn.manager.save(person);

  console.log("Saved " + person.id);
}

const app = express();
app.use(express.json());

app.get("/", (req, res) => res.send("Express + TypeScript Server"));

app.listen(4000, () => {
  console.log(`Server is listening on port 4000`);
});
