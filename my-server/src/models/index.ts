import { User } from "./user";
import { Photo } from "./photo";
import { Person } from "my-models";

export * from "./user";
export * from "./photo";
export const models = [User, Photo, Person];
