import { models } from "./src/models";

export = {
  type: "sqlite",
  database: "database.sqlite",
  synchronize: true,
  logging: false,
  entities: models,
  migrations: ["migration/**/*.ts"],
  subscribers: ["subscriber/**/*.ts"],
};
